CREATE DATABASE IF NOT EXISTS `UD22-E1`;
USE `UD22-E1`;

DROP TABLE IF EXISTS `Cliente`;

CREATE TABLE `Cliente` (
  `id` int(11) NOT NULL auto_increment,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `direccion` varchar(250) DEFAULT NULL,
  `dni` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `Cliente`(nombre, apellido, direccion, dni, fecha) VALUES ('Xavi', 'Bonet','c/ cervantes 89','47938829','2021-03-04');
INSERT INTO `Cliente`(nombre, apellido, direccion, dni, fecha) VALUES ('David', 'Bonet','c/ cervantes 89','47938828','2021-03-04');