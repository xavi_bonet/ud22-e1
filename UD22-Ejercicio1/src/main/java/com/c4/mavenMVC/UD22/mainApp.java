package com.c4.mavenMVC.UD22;

import com.c4.mavenMVC.UD22.controller.ClienteController;
import com.c4.mavenMVC.UD22.model.dao.ClienteDao;
import com.c4.mavenMVC.UD22.model.service.ClienteServ;
import com.c4.mavenMVC.UD22.view.VentanaBuscar;
import com.c4.mavenMVC.UD22.view.VentanaListar;
import com.c4.mavenMVC.UD22.view.VentanaPrincipal;
import com.c4.mavenMVC.UD22.view.VentanaRegistro;

public class mainApp {
		
		ClienteServ miClienteServ;
		VentanaPrincipal miVentanaPrincipal;
		VentanaBuscar miVentanaBuscar;
		VentanaRegistro miVentanaRegistro;
		VentanaListar miVentanaListar;
		ClienteController clienteController;
		ClienteDao miClienteDao;
		
		/**
		 * @param args
		 */
		public static void main(String[] args) {
			mainApp miPrincipal = new mainApp();
			miPrincipal.iniciar();
		}

		/**
		 * Permite instanciar todas las clases con las que trabaja
		 * el sistema
		 */
		private void iniciar() {
			
			/*Se instancian las clases*/
			miVentanaPrincipal=new VentanaPrincipal();
			miVentanaRegistro=new VentanaRegistro();
			miVentanaBuscar= new VentanaBuscar();
			miVentanaListar=new VentanaListar();
			miClienteServ=new ClienteServ();
			clienteController=new ClienteController();
			miClienteDao=new ClienteDao();
			
			/*Se establecen las relaciones entre clases*/
			miVentanaPrincipal.setCoordinador(clienteController);
			miVentanaRegistro.setCoordinador(clienteController);
			miVentanaBuscar.setCoordinador(clienteController);
			miVentanaListar.setCoordinador(clienteController);
			miClienteServ.setclienteController(clienteController);
			
			/*Se establecen relaciones con la clase coordinador*/
			clienteController.setMiVentanaPrincipal(miVentanaPrincipal);
			clienteController.setMiVentanaRegistro(miVentanaRegistro);
			clienteController.setMiVentanaBuscar(miVentanaBuscar);
			clienteController.setMiVentanaListar(miVentanaListar);
			clienteController.setClienteServ(miClienteServ);
			clienteController.setClienteDao(miClienteDao);
					
			miVentanaPrincipal.setVisible(true);
	}
}
