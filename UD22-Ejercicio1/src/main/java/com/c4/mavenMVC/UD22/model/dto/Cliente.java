package com.c4.mavenMVC.UD22.model.dto;

public class Cliente {
	
	private Integer idCliente;
	private String nombreCliente;
	private String apellidoCliente;
	private String direccionCliente;
	private Integer dniCliente;
	private String fechaCliente;

	public Cliente(){
	}
	public Cliente(Integer idCliente, String nombreCliente, String apellidoCliente, String direccionCliente, Integer dniCliente, String fechaCliente) {
		this.idCliente = idCliente;
		this.nombreCliente = nombreCliente;
		this.apellidoCliente = apellidoCliente;
		this.direccionCliente = direccionCliente;
		this.dniCliente = dniCliente;
		this.fechaCliente = fechaCliente;
	}

	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getApellidoCliente() {
		return apellidoCliente;
	}
	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}
	public String getDireccionCliente() {
		return direccionCliente;
	}
	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	public Integer getDniCliente() {
		return dniCliente;
	}
	public void setDniCliente(Integer dniCliente) {
		this.dniCliente = dniCliente;
	}
	public String getFechaCliente() {
		return fechaCliente;
	}
	public void setFechaCliente(String fechaCliente) {
		this.fechaCliente = fechaCliente;
	}

}
