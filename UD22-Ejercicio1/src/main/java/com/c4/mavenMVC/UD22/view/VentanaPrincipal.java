package com.c4.mavenMVC.UD22.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.c4.mavenMVC.UD22.controller.ClienteController;

public class VentanaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private ClienteController clienteController; //objeto PersonaController que permite la relacion entre esta clase y la clase PersonaController
	private JLabel labelTitulo;
	private JButton botonRegistrar,botonBuscar,botonListar;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana principal
	 */
	public VentanaPrincipal() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		botonRegistrar = new JButton();
		botonRegistrar.setBounds(50, 52, 120, 25);
		botonRegistrar.setText("Registrar");
		
		botonBuscar = new JButton();
		botonBuscar.setBounds(50, 88, 120, 25);
		botonBuscar.setText("Buscar");

		botonListar = new JButton();
		botonListar.setBounds(50, 124, 120, 25);
		botonListar.setText("Listar");
		
		labelTitulo = new JLabel();
		labelTitulo.setText("UD22 - Ejercicio 1");
		labelTitulo.setBounds(33, 11, 164, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 15));

		botonRegistrar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonListar.addActionListener(this);
		
		getContentPane().add(botonBuscar);
		getContentPane().add(botonRegistrar);
		getContentPane().add(labelTitulo);
		getContentPane().add(botonListar);
		
		setSize(225, 225);
		setTitle("UD22");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

	}


	public void setCoordinador(ClienteController clienteController) {
		this.clienteController=clienteController;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonRegistrar) {
			clienteController.mostrarVentanaRegistro();			
		}
		if (e.getSource()==botonBuscar) {
			clienteController.mostrarVentanaConsulta();			
		}
		if (e.getSource()==botonListar) {
			clienteController.mostrarVentanaListar();			
		}
	}
}
