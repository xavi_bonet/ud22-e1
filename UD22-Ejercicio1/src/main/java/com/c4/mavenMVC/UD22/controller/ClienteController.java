package com.c4.mavenMVC.UD22.controller;

import java.util.ArrayList;

import com.c4.mavenMVC.UD22.model.dao.ClienteDao;
import com.c4.mavenMVC.UD22.model.dto.Cliente;
import com.c4.mavenMVC.UD22.model.service.ClienteServ;
import com.c4.mavenMVC.UD22.view.VentanaBuscar;
import com.c4.mavenMVC.UD22.view.VentanaListar;
import com.c4.mavenMVC.UD22.view.VentanaPrincipal;
import com.c4.mavenMVC.UD22.view.VentanaRegistro;

public class ClienteController {

	private ClienteServ clienteServ;
	private ClienteDao clienteDao;
	private VentanaPrincipal miVentanaPrincipal;
	private VentanaRegistro miVentanaRegistro;
	private VentanaBuscar miVentanaBuscar;
	private VentanaListar miVentanaListar;

	//Metodos getter Setters de vistas
	public VentanaPrincipal getMiVentanaPrincipal() {
		return miVentanaPrincipal;
	}
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	public VentanaRegistro getMiVentanaRegistro() {
		return miVentanaRegistro;
	}
	public void setMiVentanaRegistro(VentanaRegistro miVentanaRegistro) {
		this.miVentanaRegistro = miVentanaRegistro;
	}
	public VentanaBuscar getMiVentanaBuscar() {
		return miVentanaBuscar;
	}
	public void setMiVentanaBuscar(VentanaBuscar miVentanaBuscar) {
		this.miVentanaBuscar = miVentanaBuscar;
	}
	public VentanaListar getMiVentanaListar() {
		return miVentanaListar;
	}
	public void setMiVentanaListar(VentanaListar miVentanaListar) {
		this.miVentanaListar = miVentanaListar;
	}
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}
	public ClienteDao getClienteDao() {
		return clienteDao;
	}
	public void setClienteDao(ClienteDao clienteDao) {
		this.clienteDao = clienteDao;
	}
	
	//Hace visible las vistas de Registro, Consulta y Listar
	public void mostrarVentanaRegistro() {
		miVentanaRegistro.setVisible(true);
	}
	public void mostrarVentanaConsulta() {
		miVentanaBuscar.setVisible(true);
	}
	public void mostrarVentanaListar() {
		miVentanaListar.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCliente(Cliente miCliente) {
		clienteServ.validarRegistro(miCliente);
	}
	
	public Cliente buscarCliente(String codigoCliente) {
		return clienteServ.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Cliente miCliente) {
		clienteServ.validarModificacion(miCliente);
	}
	
	public void eliminarCliente(String codigo) {
		clienteServ.validarEliminacion(codigo);
	}
	
	public ArrayList<Cliente> listarClientes() {
		return clienteDao.listarClientes();
	}
	
}
