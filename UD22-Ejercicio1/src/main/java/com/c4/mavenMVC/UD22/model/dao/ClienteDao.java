package com.c4.mavenMVC.UD22.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import com.c4.mavenMVC.UD22.model.conexion.ConnectionDB;
import com.c4.mavenMVC.UD22.model.dto.Cliente;

public class ClienteDao {

	public void registrarCliente(Cliente cliente) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			//Fecha del sistema para hacer el update
		    long millis=System.currentTimeMillis();   
		    Date dt = new Date(millis);
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			// Ejecutar Query que insrta el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E1",
							"INSERT INTO `cliente`(nombre, apellido, direccion, dni, fecha) VALUES ('"
							+ cliente.getNombreCliente() + "', '" + cliente.getApellidoCliente() + "','"
							+ cliente.getDireccionCliente() + "','" + cliente.getDniCliente() + "','"
							+ sdf.format(dt) + "');");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente", "Información",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha registrado", "Información", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	public ArrayList<Cliente> listarClientes() {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
			

			// Ejecutar Query que obtiene el cliente, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cliente
			ResultSet res = conex.getValues("UD22-E1", "SELECT * FROM cliente;");
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				Cliente cliente = new Cliente();
				cliente.setIdCliente(Integer.parseInt(res.getString("id")));
				cliente.setNombreCliente(res.getString("nombre"));
				cliente.setApellidoCliente(res.getString("apellido"));
				cliente.setDireccionCliente(res.getString("direccion"));
				cliente.setDniCliente(Integer.parseInt(res.getString("dni")));
				cliente.setFechaCliente(res.getString("fecha"));
				listaClientes.add(cliente);
			}
			// Cerrar conexion
			conex.closeConnection();
			
			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return listaClientes;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	public Cliente buscarCliente(int codigo) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		boolean existe = false;
		try {
			Cliente cliente = new Cliente();

			// Ejecutar Query que obtiene el cliente, guardar los datos en el resultSet i
			// guardarlos despues en el objeto cliente
			ResultSet res = conex.getValues("UD22-E1", "SELECT * FROM cliente where id = '" + codigo + "';");
			
			// Guardar los datos en el objeto cliente
			while (res.next()) {
				existe = true;
				cliente.setIdCliente(Integer.parseInt(res.getString("id")));
				cliente.setNombreCliente(res.getString("nombre"));
				cliente.setApellidoCliente(res.getString("apellido"));
				cliente.setDireccionCliente(res.getString("direccion"));
				cliente.setDniCliente(Integer.parseInt(res.getString("dni")));
				cliente.setFechaCliente(res.getString("fecha"));
			}
			// Cerrar conexion
			conex.closeConnection();

			// Si el cliente no existe, lanzar un error
			if (!existe)
				throw new Exception();
			
			return cliente;
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			return null;
		}
	}

	public void modificarCliente(Cliente cliente) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			//Fecha del sistema para hacer el update
		    long millis=System.currentTimeMillis();   
		    Date dt = new Date(millis);
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			// Ejecutar Query que modifica el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E1",
							"update cliente set nombre = '" + cliente.getNombreCliente() + "', apellido = '"
							+ cliente.getApellidoCliente() + "', direccion = '" + cliente.getDireccionCliente()
							+ "', dni = '" + cliente.getDniCliente() + "',fecha = '" + sdf.format(dt)
							+ "' where id = '" + cliente.getIdCliente() + "';");

			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
				
			// Cerrar conexion
			conex.closeConnection();
			
			JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ", "Confirmación",JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
			System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Error al Modificar", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void eliminarCliente(String codigo) {
		// Abrir la conexion a la base de datos
		ConnectionDB conex = new ConnectionDB();
		try {
			boolean resultadoQuery;

			// Ejecutar Query que modifica el cliente en la base de datos
			resultadoQuery = conex.executeQuery("UD22-E1", "DELETE FROM cliente WHERE id='" + codigo + "'");
			
			// Si devuelve false lanzamos un error!
			if (resultadoQuery == false) {
				throw new Exception();
			}
			
			// Cerrar conexion
			conex.closeConnection();
			
            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
            
		} catch (Exception e) {
			// Cerrar conexion
			conex.closeConnection();
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}

}
